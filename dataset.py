"""
Requires Pandas
Handles the loading of datasets, as well as any merging that needs to be done on the DataFrames
"""
import pandas as pd
import numpy as np

"""
Get the geography data.
"""
def get_geography():
    return pd.read_csv("Lat_Lon/world_country_and_usa_states_latitude_and_longitude_values.csv")

"""
Get the country data.
"""
def get_country():
    return get_geography().drop(["usa_state_code", "usa_state_latitude", "usa_state_longitude", "usa_state"], axis=1)

"""
Get the population data.
"""
def get_population():
    return pd.read_csv("Population/world_population.csv")

"""
Get a Dataframe for each year of the WHR.
"""
def get_yearly():
    yearly_df = {}
    for year in range(2015, 2023):
        filename = "WHR_2022/" + str(year) + ".csv"
        yearly_df[year] = pd.read_csv(filename)
    return yearly_df

"""
Get all the years of the WHR with consistent columns.
The columns will be the weighted score components of the WHR.
"""
def get_consistent_yearly():
    yearly_df = {}
    for year in range(2015, 2023):
        filename = "WHR_2022/" + str(year) + ".csv"
        yearly_df[year] = pd.read_csv(filename)

    yearly_df[2022] = yearly_df[2022].drop(['Whisker-high','Whisker-low','Dystopia (1.83) + residual'],axis = 1)
    yearly_df[2022].columns = ["Overall rank", "Country or region","Score","GDP per capita",'Social support',
                 "Healthy life expectancy","Freedom to make life choices",
                 "Generosity","Perceptions of corruption"]

    yearly_df[2021] = yearly_df[2021].drop(['Regional indicator','Standard error of ladder score','upperwhisker','lowerwhisker','Logged GDP per capita','Social support','Healthy life expectancy','Freedom to make life choices','Generosity','Perceptions of corruption','Ladder score in Dystopia', 'Dystopia + residual'],axis = 1)
    yearly_df[2021].columns = ["Country or region","Score","GDP per capita",'Social support',
                 "Healthy life expectancy","Freedom to make life choices",
                 "Generosity","Perceptions of corruption"]
    yearly_df[2021]["Overall rank"] = np.argsort(yearly_df[2021]['Score'])[::-1].values+1

    yearly_df[2020] = yearly_df[2020].drop(['Regional indicator','Standard error of ladder score','upperwhisker','lowerwhisker','Logged GDP per capita','Social support','Healthy life expectancy','Freedom to make life choices','Generosity','Perceptions of corruption','Ladder score in Dystopia', 'Dystopia + residual'],axis = 1)
    yearly_df[2020].columns = ["Country or region","Score","GDP per capita",'Social support',
                 "Healthy life expectancy","Freedom to make life choices",
                 "Generosity","Perceptions of corruption"]
    yearly_df[2020]["Overall rank"] = np.argsort(yearly_df[2020]['Score'])[::-1].values+1

    yearly_df[2017] = yearly_df[2017].drop(["Whisker.high","Whisker.low","Dystopia.Residual"], axis = 1)
    yearly_df[2017].columns = ["Country or region","Overall rank","Score","GDP per capita",'Social support',
                 "Healthy life expectancy","Freedom to make life choices",
                 "Generosity","Perceptions of corruption"]

    yearly_df[2016] = yearly_df[2016].drop(['Region','Lower Confidence Interval','Upper Confidence Interval', 'Dystopia Residual'],axis = 1)
    yearly_df[2016].columns = ["Country or region","Overall rank","Score","GDP per capita",'Social support',
                 "Healthy life expectancy","Freedom to make life choices",
                 "Perceptions of corruption","Generosity"]

    yearly_df[2015] = yearly_df[2015].drop(["Region",'Standard Error','Dystopia Residual'],axis=1)
    yearly_df[2015].columns = ["Country or region","Overall rank","Score","GDP per capita",'Social support',
                 "Healthy life expectancy","Freedom to make life choices",
                 "Perceptions of corruption","Generosity"]

    for year in range(2015, 2023):
        yearly_df[year]['year'] = year
    total = pd.concat(yearly_df.values(),ignore_index=True)
    return total

"""
Get the merged dataframe, that joins each dataframe by the Country column.
"""
def get_full():
    yearly_df = get_yearly()
    merged_df = pd.merge(get_country(), get_population(), how="inner", left_on="country", right_on="Country/Territory")
    merged_df = merged_df.drop(["Country/Territory"], axis=1)
    for year in range(2015, 2023):
        renamed_cols = [str(year) + " " + col for col in yearly_df[year].columns]
        yearly_df[year].columns = renamed_cols
        country_col = ":)"
        for col in yearly_df[year].columns:
            if "Country" in col:
                country_col = col
        merged_df = pd.merge(merged_df, yearly_df[year], how="inner", left_on="country", right_on=country_col)
        merged_df = merged_df.drop([country_col], axis=1)
    return merged_df

"""
Get the aggregate data.
This includes unweighted data from Gallup world poll.
"""
def get_aggregate():
    return pd.read_csv("WHR_2022/Aggregate.csv")