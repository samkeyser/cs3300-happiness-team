# World Happines Report: CS 3300 Final

## Members

- Mitchell Johnstone
- Sam Keyser
- Jonathan Keane


# Submission Structure

- `./HappinessPresentaion.pptx` - Presentation Powerpoint

- `./PDFS` - Jupyter Notebook PDFs

- All other materials in the root directory include our dataset, Jupyter notebooks, and code that we used for loading our cleaned data for all of our notebooks (created by Mitchell Johnstone)


# Research Questions

We did not have a main hypothesis to test with our project, but we had several objectives we wanted to accomplish to understand this data set further. To frame our objectives for this project, we restate them here:

1. Understand which features, between the 6 factors focused on by the Gallup survey and the features we pulled in with our additional datasets, contribute the most to happiness.

    - *This is accomplished in the feature importance section of this notebook*

2. Determine if the most important features change over the years, and make a guess as to why.

    - *This is accomplished in the feature importance section of this notebook as well*
    
3. Determine which regions and continents are the happiest across the years.

    - *This is done across our EDA noteboks (EDA_SK.ipynb and EDA_MJ.ipynb)*

4. Determine which latitude and longitude ranges are the happiest (use mean happiness over the years in the happiness reports).

    - *This is done in our EDA notebooks, specifically Jonny's (EDA_JK.ipynb)*

5. Develop a predictive model for Happiness Score based on the main factors to predict the Happiness score for a naive country.

    - *This work is done in this notebook (regression) as well as in our categorical model notebook (HappinessTeam_CategoricalModel.ipynb) and our PCA notebook (HappinessTeam_CategoricalModel.ipynb)*
    
    
# Dataset: World Happines Report (2025-2022)

We did a lot of our data cleaning in ```dataset.py``` and got all features on a yearly basis for the same 6 features:

- `Country/Region`: The country that is being analyzed.
- `Overall Rank`: The ranking of the country in the happiness index.
- `Score`: The composite happiness score of the country (range: 0-10)
- `GDP per capita`: The GPD per capita score of the country
- `Social Support`: A likelihood of whether society would help someone in trouble
- `Healthy Life Expectancy`: A scaled version of the life expectancy of the country.
- `Freedom to make life choices`: How likely a person is to feel like they have freedom.
- `Perceptions of corruption`: A scale of how trustworthy the government and general businesses are
- `Generosity`: How much people give to charity, scaled by the country's GDP
- `year`: The year of the analysis of the country.
- `Dystopia / Residual` (Excluded because we do not know what impacts this residual factor, and it was a large component of the summation to get the happiness score)

These features come from the dataset created by the Gallup Corporation, who made a worldwide survey to try and understand happiness worldwie. The Gallup corportation polls countries across the world every year to try and score these countries, and we have data for the survey results from 2015 to 2022. The target feature for this dataset is the score value, which is a summation of the main scores (ranging up to a score of about 8) for different features that are evaluated in the Gallup poll. 

***Resources:***

- [WHR Dataset](https://www.kaggle.com/datasets/unsdsn/world-happiness)

- [WHR Website](https://worldhappiness.report/)

- [Gallup World Poll](https://www.gallup.com/analytics/318875/global-research.aspx)
